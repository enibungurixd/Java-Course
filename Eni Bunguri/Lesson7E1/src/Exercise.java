import java.util.Random;

public class Exercise {

    public static int sum(int n, int m, int[][] ar){
        int sum=0;

        for (int i=0;i<n;i++){

            for (int j=0;j<m;j++){

                sum+=ar[i][j];
            }
        }
        return sum;
    }
    public static int max(int n, int m, int[][] ar){
    int max = ar[0][0];
    for(int i =0 ; i<n; i++){
        for(int j = 0; j<m; j++){
            if(ar[i][j] > max){
                max = ar[i][j];
            }
        }
    }
    return max;
    }
    public static void main(String[] args){
        Random r = new Random();
        int[][] arr = new int[6][6];
        for(int i = 0; i<6; i++){
            for(int j = 0; j<6; j++){
                arr[i][j] = r.nextInt(15);

            }

        }
    System.out.println(max(6,6,arr));
        System.out.println(sum(6,6, arr));
    }
}