
import java.awt.*;
import java.util.ArrayList;

public class Main {

    public static double getArea(Rectangle rectangle) {

        return rectangle.getWidth() * rectangle.getHeight();
    }

    public static void main(String[] args) {
        ArrayList<Rectangle> rectangles = new ArrayList<>();
        Rectangle r1 = new Rectangle(0, 0, 50, 10);
        Rectangle r2 = new Rectangle(0, 0, 50, 80);
        Rectangle r3 = new Rectangle(0, 0, 10, 40);
        Rectangle r4 = new Rectangle(0, 0, 500, 10);
        Rectangle r5 = new Rectangle(0, 0, 100, 10);

        rectangles.add(r1);
        rectangles.add(r2);
        rectangles.add(r3);
        rectangles.add(r4);
        rectangles.add(r5);

        Double[] area = new Double[5];
        int i = 0;

        for (Rectangle tmp : rectangles) {
            area[i] = getArea(tmp);
            i++;
        }

        double min = area[0];
        int poz = 0;

        for (i = 0; i < 5; i++) {
            System.out.println(area[i]);
            if (area[i] < min) {

                min = area[i];
                poz = i;

            }
            System.out.println()
        }

        rectangles.remove(poz);
        System.out.println(rectangles.size());
    }
}