public class Exercise1
{
    private String city;
    private String street;
    private String phoneNumber;
    private String faxNumber;

    public Exercise1(){

}
    public String getCity(){

        return city;
    }
    public String getStreet(){

        return street;
    }
    public String getPhone(){
        return phoneNumber;
    }
    public String getFax(){
        return faxNumber;
    }
}


// Separate the getter and setter methods. 
// Thanks for the review, I also had forgotten to declare the constructor.