public class Financial {


    public static double getTax(double taxRate, double amount){
        return taxRate*amount;
    }
}

class FinancialTester{
    public static void main(String[] args){
        double tax1 = Financial.getTax(8.6/100.0, 250);
        System.out.println("The tax for the following price is: " + tax1);
    }
}