import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class GUI extends JFrame{

    private ShoppingCart shoppingCart;

    private JTextArea area;

    private JPanel p;
    private JPanel p1;
    private JPanel p2;
    private JPanel p3;
    private JPanel p4;
    private JPanel p5;
    private JPanel p6;

    private int count1 = 0;
    private int count2 = 0;
    private int count3 = 0;
    private int count4 = 0;
    private int count5 = 0;
    private int count6 = 0;

    private JButton b1;
    private JButton b2;
    private JButton b3;
    private JButton b4;
    private JButton b5;
    private JButton b6;

    private JButton buy;
    private JButton reset;

    private JButton bb1;
    private JButton bb2;
    private JButton bb3;
    private JButton bb4;
    private JButton bb5;
    private JButton bb6;

    private JLabel l1;
    private JLabel l2;
    private JLabel l3;
    private JLabel l4;
    private JLabel l5;
    private JLabel l6;

    private JLabel price1;
    private JLabel price2;
    private JLabel price3;
    private JLabel price4;
    private JLabel price5;
    private JLabel price6;

    public GUI() {
         p = new JPanel();
        p1 = new JPanel();
        p2 = new JPanel();
        p3 = new JPanel();
        p4 = new JPanel();
        p5 = new JPanel();
        p6 = new JPanel();

        l1 = new JLabel("1)Quantity 0");
        l2 = new JLabel("2)Quantity 0");
        l3 = new JLabel("3)Quantity 0");
        l4 = new JLabel("4)Quantity 0");
        l5 = new JLabel("5)Quantity 0");
        l6 = new JLabel("6)Quantity 0");

        b1 = new JButton("+");
        bb1 = new JButton("-");
        b2 = new JButton("+");
        bb2 = new JButton("-");
        b3 = new JButton("+");
        bb3 = new JButton("-");
        b4 = new JButton("+");
        bb4 = new JButton("-");
        b5 = new JButton("+");
        bb5 = new JButton("-");
        b6 = new JButton("+");
        bb6 = new JButton("-");

        area = new JTextArea();
        area.setEditable(false);


        price1 = new JLabel("Price: 85$");
        price2 = new JLabel("Price: 95$");
        price3 = new JLabel("Price: 112$");
        price4 = new JLabel("Price: 180$");
        price5 = new JLabel("Price: 120$");
        price6 = new JLabel("Price: 125$");

        shoppingCart = new ShoppingCart();
        buy = new JButton("BUY!");
        reset = new JButton("RESET!");
        p1.add(l1);
        p1.add(price1);
        p1.add(b1);
        p1.add(bb1);

        p2.add(l2);
        p2.add(price2);
        p2.add(b2);
        p2.add(bb2);

        p3.add(l3);
        p3.add(price3);
        p3.add(b3);
        p3.add(bb3);

        p4.add(l4);
        p4.add(price4);
        p4.add(b4);
        p4.add(bb4);

        p5.add(l5);
        p5.add(price5);
        p5.add(b5);
        p5.add(bb5);

        p6.add(l6);
        p6.add(price6);
        p6.add(b6);
        p6.add(bb6);

        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);
        p.add(p5);
        p.add(p6);

        setLayout(new GridLayout(5,1));
        add(shoppingCart);
        add(p);
        add(buy);
        add(reset);
        add(area);

        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(count1!=100) {
                    count1++;
                    l1.setText("1)Quantity: " + count1);
                }
            }
        });

        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(count2!=100) {
                    count2++;
                    l2.setText("2)Quantity " + count2);
                }
            }
        });

        b3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(count3!=100) {
                    count3++;
                    l3.setText("3)Quantity: " + count3);
                }
            }
        });

        b4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(count4!=100) {
                    count4++;
                    l4.setText("4)Quantity: " + count4);
                }
            }
        });

        b5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(count5!=100) {
                    count5++;
                    l5.setText("5)Quantity: " + count5);
                }
            }
        });

        b6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(count6!=100) {
                    count6++;
                    l6.setText("6)Quantity: " + count6);
                }
            }
        });

        bb1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(count1>0){
                    count1--;
                    l1.setText("1)Quantity: " + count1);
                }
            }
        });

        bb2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(count2>0){
                    count2--;
                    l2.setText("2)Quantity: " + count2);
                }
            }
        });

        bb3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(count3>0){
                    count3--;
                    l3.setText("3)Quantity: " + count3);
                }
            }
        });

        bb4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(count4>0){
                    count4--;
                    l4.setText(("4)Quantity:" + count4));
                }
            }
        });

        bb5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(count5>0){
                    count5--;
                    l5.setText("5)Quantity: " + count5);
                }
            }
        });

        bb6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(count6>0){
                    count6--;
                    l6.setText("6)Quantity: " + count1);
                }
            }
        });


        buy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                area.setText(l1.getText()+"*"+count1+"="+(85*count1)+"$"+"\n");
                area.append(l2.getText()+"*"+count2+"="+(95*count2)+"$"+"\n");
                area.append(l3.getText()+"*"+count3+"="+(112*count3)+"$"+"\n");
                area.append(l4.getText()+"*"+count4+"="+(180*count4+"$"+"\n"));
                area.append(l5.getText()+"*"+count5+"="+(120*count5)+"$"+"\n");
                area.append(l6.getText() + "*" + count6 +"="+(125*count6) +"$"+"\n");
                area.append("Total purchase: "+((85*count1)+(95*count2)+(112*count3)+(180*count4)+(120*count5)+(125*count6)) +"$");
            }
        });

        reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                l1.setText("1)Quantity: 0" );
                l2.setText("2)Quantity: 0" );
                l3.setText("3)Quantity: 0" );
                l4.setText("4)Quantity: 0" );
                l5.setText("5)Quantity: 0" );
                l6.setText("6)Quantity: 0" );

                count1 = 0;
                count2 = 0;
                count3 = 0;
                count4 = 0;
                count5 = 0;
                count6 = 0;

                area.setText("");

            }
        });

    }
}