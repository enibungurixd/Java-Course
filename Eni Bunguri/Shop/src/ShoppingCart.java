import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ShoppingCart extends JComponent {
    private ImageIcon imageIcon1;
    private ImageIcon imageIcon2;
    private ImageIcon imageIcon3;
    private ImageIcon imageIcon4;
    private ImageIcon imageIcon5;
    private ImageIcon imageIcon6;

    private Image image1;
    private Image image2;
    private Image image3;
    private Image image4;
    private Image image5;
    private Image image6;

    private Image newimg1;
    private Image newimg2;
    private Image newimg3;
    private Image newimg4;
    private Image newimg5;
    private Image newimg6;



    public ShoppingCart() {
        imageIcon1 = new ImageIcon("/home/e.bunguri/Desktop/img/1.jpg"); // load the image to a imageIcon
        image1 = imageIcon1.getImage(); // transform it
        newimg1 = image1.getScaledInstance(280, 280, java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
        imageIcon1 = new ImageIcon(newimg1);

        imageIcon2 = new ImageIcon("/home/e.bunguri/Desktop/img/2.jpg"); // load the image to a imageIcon
        image2 = imageIcon2.getImage(); // transform it
        newimg2 = image2.getScaledInstance(280, 280, java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
        imageIcon2 = new ImageIcon(newimg2);

        imageIcon3 = new ImageIcon("/home/e.bunguri/Desktop/img/3.jpg"); // load the image to a imageIcon
        image3 = imageIcon3.getImage(); // transform it
        newimg3 = image3.getScaledInstance(280, 280, java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
        imageIcon3 = new ImageIcon(newimg3);

        imageIcon4 = new ImageIcon("/home/e.bunguri/Desktop/img/4.jpg"); // load the image to a imageIcon
        image4 = imageIcon4.getImage(); // transform it
        newimg4 = image4.getScaledInstance(280, 280, java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
        imageIcon4 = new ImageIcon(newimg4);

        imageIcon5 = new ImageIcon("/home/e.bunguri/Desktop/img/5.jpg"); // load the image to a imageIcon
        image5 = imageIcon5.getImage(); // transform it
        newimg5 = image5.getScaledInstance(280, 280, java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
        imageIcon5 = new ImageIcon(newimg5);

        imageIcon6 = new ImageIcon("/home/e.bunguri/Desktop/img/6.jpg"); // load the image to a imageIcon
        image6 = imageIcon6.getImage(); // transform it
        newimg6 = image6.getScaledInstance(280, 280, java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
        imageIcon6 = new ImageIcon(newimg6);




    }

    public void paint(Graphics g) {
        imageIcon1.paintIcon(super.getParent(), g, 150, 0);
        imageIcon2.paintIcon(super.getParent(), g, 330+100, 0);
        imageIcon3.paintIcon(super.getParent(), g, 620+100, 0);
        imageIcon4.paintIcon(super.getParent(), g, 910+100, 0);
        imageIcon5.paintIcon(super.getParent(), g, 1200+100, 0);
        imageIcon6.paintIcon(super.getParent(), g, 1490+100, 0);

    }
}