package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI extends JFrame {


    private Foto foto;
    private JLabel mascara;
    private JLabel eyeliner;
    private JLabel highlighter;
    private JLabel lipstick;
    private JLabel foundation;


    private JButton m1;
    private JButton p1;
    private JButton m2;
    private JButton p2;
    private JButton m3;
    private JButton p3;
    private JButton m4;
    private JButton p4;
    private JButton m5;
    private JButton p5;

    private JButton buy;
    private JButton reset;

    private JPanel pn1;
    private JPanel pn2;
    private JPanel pn3;
    private JPanel pn4;
    private JPanel pn5;
    private JPanel panel;
    private JPanel empty_panel;
    private JPanel buttons;

    private int count1;
    private int count2;
    private int count3;
    private int count4;
    private int count5;

    private  JLabel mascara_price;
    private  JLabel eyeliner_price;
    private  JLabel highlighter_price;
    private  JLabel lipstick_price;
    private  JLabel foundation_price;

    private JTextArea area;


    public GUI(){
        foto=new Foto();


        mascara=new JLabel("Quantity: 0");
        eyeliner=new JLabel("Quantity: 0");
        highlighter=new JLabel("Quantity: 0");
        lipstick=new JLabel("Quantity: 0");
        foundation=new JLabel("Quantity: 0");

        mascara_price=new JLabel("Mascara price: 800");
        eyeliner_price=new JLabel("Eyeliner price: 700");
        highlighter_price=new JLabel("Highlighter price: 600");
        lipstick_price=new JLabel("Lipstick price: 500");
        foundation_price=new JLabel("Foundation price: 1000");

        m1=new JButton("-");
        p1=new JButton("+");
        m2=new JButton("-");
        p2=new JButton("+");
        m3=new JButton("-");
        p3=new JButton("+");
        m4=new JButton("-");
        p4=new JButton("+");
        m5=new JButton("-");
        p5=new JButton("+");

        buy=new JButton("BUY");

        reset=new JButton(" RESET");

        pn1=new JPanel();
        pn2=new JPanel();
        pn3=new JPanel();
        pn4=new JPanel();
        pn5=new JPanel();
        panel=new JPanel();
        empty_panel=new JPanel();
        buttons =new JPanel();

        pn1.setLayout(new GridLayout(4,1));
        pn1.add(mascara_price);
        pn1.add(mascara);
        pn1.add(m1);
        pn1.add(p1);

        pn2.setLayout(new GridLayout(4,1));
        pn2.add(eyeliner_price);
        pn2.add(eyeliner);
        pn2.add(m2);
        pn2.add(p2);

        pn3.setLayout(new GridLayout(4,1));
        pn3.add(highlighter_price);
        pn3.add(highlighter);
        pn3.add(m3);
        pn3.add(p3);

        pn4.setLayout(new GridLayout(4,1));
        pn4.add(lipstick_price);
        pn4.add(lipstick);
        pn4.add(m4);
        pn4.add(p4);

        pn5.setLayout(new GridLayout(4,1));
        pn5.add(foundation_price);
        pn5.add(foundation);
        pn5.add(m5);
        pn5.add(p5);

        buttons.add(buy);
        buttons.add(reset);

        panel.setLayout(new GridLayout(6,1));
        panel.add(pn1);
        panel.add(pn2);
        panel.add(pn3);
        panel.add(pn4);
        panel.add(pn5);
        panel.add(buttons);

        area=new JTextArea();
        area.setEditable(false);

        setLayout(new GridLayout(1,4));
        add(foto);
        add(panel);
        add(empty_panel);
        add(area);


        m1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(count1>0) {
                    count1--;
                    mascara.setText("Quantity: " + count1);
                }
            }
        });
        p1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (count1<100) {
                    count1++;
                    mascara.setText("Quantity: " + count1);
                }
            }
        });

        m2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (count2>0) {
                    count2--;
                    eyeliner.setText("Quantity: " + count2);
                }
            }
        });
        p2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (count2<100) {
                    count2++;
                    eyeliner.setText("Quantity: " + count2);
                }
            }
        });

        m3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (count3>0) {
                    count3--;
                    highlighter.setText("Quantity: " + count3);
                }
            }
        });
        p3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(count3<100) {
                    count3++;
                    highlighter.setText("Quantity: " + count3);
                }
            }
        });

        m4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (count4>0) {
                    count4--;
                    lipstick.setText("Quantity: " + count4);
                }
            }
        });
        p4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (count4<100) {
                    count4++;
                    lipstick.setText("Quantity: " + count4);
                }
            }
        });

        m5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (count5>0) {
                    count5--;
                    foundation.setText("Quantity: " + count5);
                }
            }
        });
        p5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (count5<100) {
                    count5++;
                    foundation.setText("Quantity: " + count5);
                }
            }
        });

        buy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                area.setText("1. "+mascara_price.getText()+"*"+count1+"="+(800*count1)+"\n");
                area.append("2.  " +eyeliner_price.getText()+"*"+count2+"="+(700*count2)+"\n");
                area.append("3. " +highlighter_price.getText()+"*"+count3+"="+(600*count3)+"\n");
                area.append("4. " +lipstick_price.getText()+"*"+count4+"="+(500*count4+"\n"));
                area.append("5. " +foundation_price.getText()+"*"+count5+"="+(1000*count5)+"\n");
                area.append("Total purchase: "+((800*count1)+(700*count1)+(600*count1)+(500*count1)+(1000*count1)));
            }
        });

        reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                mascara.setText("Quantity: 0");
                eyeliner.setText("Quantity: 0");
                lipstick.setText("Quantity: 0");
                highlighter.setText("Quantity: 0");
                foundation.setText("Quantity: 0");

                count1=0;
                count2=0;
                count3=0;
                count4=0;
                count5=0;

                area.setText("");
            }
        });

    }
}
