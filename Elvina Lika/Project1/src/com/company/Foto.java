package com.company;

import javax.swing.*;
import java.awt.*;

public class Foto extends JComponent{
    private ImageIcon img;
    private ImageIcon img1;
    private ImageIcon img2;
    private ImageIcon img3;
    private ImageIcon img4;



    public Foto(){

        img=new ImageIcon("/home/e.lika/Desktop/lipstic.jpeg");
        Image image=img.getImage();
        Image newImg=image.getScaledInstance(150,150,java.awt.Image.SCALE_SMOOTH);
        img = new ImageIcon(newImg);

        img1=new ImageIcon("/home/e.lika/Desktop/eyeliner.jpg");
        Image image1=img1.getImage();
        Image newImg1=image1.getScaledInstance(150,150,java.awt.Image.SCALE_SMOOTH);
        img1 = new ImageIcon(newImg1);

        img2=new ImageIcon("/home/e.lika/Desktop/mascara.jpeg");
        Image image2=img2.getImage();
        Image newImg2=image2.getScaledInstance(150,150,java.awt.Image.SCALE_SMOOTH);
        img2 = new ImageIcon(newImg2);

        img3=new ImageIcon("/home/e.lika/Desktop/highlighter.jpeg");
        Image image3=img3.getImage();
        Image newImg3=image3.getScaledInstance(150,150,java.awt.Image.SCALE_SMOOTH);
        img3 = new ImageIcon(newImg3);

        img4=new ImageIcon("/home/e.lika/Desktop/foundations.jpg");
        Image image4=img4.getImage();
        Image newImg4=image4.getScaledInstance(150,150,java.awt.Image.SCALE_SMOOTH);
        img4 = new ImageIcon(newImg4);


    }

    public void paint(Graphics g){
        img.paintIcon(super.getParent(),g,0,0);
        img1.paintIcon(super.getParent(),g,0,180);
        img2.paintIcon(super.getParent(),g,0,360);
        img3.paintIcon(super.getParent(),g,0,540);
        img4.paintIcon(super.getParent(),g,0,720);
    }

}
