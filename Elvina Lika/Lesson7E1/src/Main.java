public class Main {

    public static int sum(int n, int m, int[][] num){
        int sum=0;
        for (int i=0;i<n;i++){
            for (int j=0;j<m;j++){
                sum+=num[i][j];
            }
        }
        return sum;
    }

    public static int max(int n, int m, int[][] num){
        int max=num[0][0];
        for (int i=0;i<n;i++){
            for (int j=0;j<m;j++){
                if (num[i][j]>max)
                    max=num[i][j];
            }
        }
        return max;
    }


    public static void main(String [] args){

        int[][] a=new int[2][3];
        a[0][0]=1;
        a[0][1]=2;
        a[0][2]=3;
        a[1][0]=4;
        a[1][1]=5;
        a[1][2]=6;
        System.out.println(sum(2,3,a));
        System.out.println(max(2,3,a));

    }

}
