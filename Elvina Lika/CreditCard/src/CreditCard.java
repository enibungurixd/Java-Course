public class CreditCard {

    private double balance;

    public CreditCard(){
        balance=0;
    }

    public void deposit(double amount){
        if(amount > 0){
            balance+=amount;
        }
    }

    public void withdraw(double amount){
        if(amount <= balance && amount >0){
            balance-=amount;
        }
    }

    public double getBalance(){
        return balance;
    }
}
