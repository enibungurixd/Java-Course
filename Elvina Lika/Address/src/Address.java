public class Address {

    private String city;
    private String street;
    private int phone;
    private int fax;

    public Address(String city, String street, int phone, int fax){
        this.city=city;
        this.street=street;
        this.phone=phone;
        this.fax=fax;
    }

    public String getCity(){
        return city;
    }

    public String getStreet(){
        return street;
    }

    public int getPhone(){
        return phone;
    }

    public int getFax() {
        return fax;
    }
}
